import { HeroesMarvelPage } from './app.po';

describe('heroes-marvel App', function() {
  let page: HeroesMarvelPage;

  beforeEach(() => {
    page = new HeroesMarvelPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
